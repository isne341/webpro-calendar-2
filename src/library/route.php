<?php
class route extends instance
{
    private $route;

    public function __construct($route_array)
    {
        parent::__construct();

        $this->route = $route_array;
    }

    private function find($route)
    {
        foreach ($this->route as $key => $value) {
            if ($key == $route) {

                $route_component = explode('/', $value);

                $file     = $route_component[0];
                $function = $route_component[1];

                return array('file' => $file, 'function' => $function);
            }
        }

        $route_component = explode('/',$this->route['404']);
        $file            = $route_component[0];
        $function        = $route_component[1];
        return array('file' => $file, 'function' => $function);
    }

    public function exec($route)
    {
        $route = $this->find($route);

        try {
            require_once 'control/' . $route['file'].'.php';

            $class    = $route['file'];
            $function = $route['function'];

            $control = new $class();

            $control->$function();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
