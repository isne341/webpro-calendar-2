<?php
class database 
{
    private $db;

    public function __construct($host, $user, $pass, $dbname, $port, $chrset = 'UTF8')
    {
        $this->db = new pdo("mysql:host=" . $host . ";dbname=" . $dbname . ";port=" . $port . ";", $user, $pass);
        $this->db->query("SET NAMES '" . $chrset . "'");
        $this->db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
    }

    public function get(){
        return $this->db;
    }

}
