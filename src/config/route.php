<?php
$config['route']['index'] = 'dashboard/index'; // show calendar
$config['route']['show'] = 'dashboard/show'; // show appointment


$config['route']['add'] = 'add/index'; // show form
$config['route']['process'] = 'add/process'; // process when you insert calendar to database
$config['route']['remove'] = 'add/process'; // process when you insert calendar to database

$config['route']['404'] = 'dashboard/index'; // file not found
